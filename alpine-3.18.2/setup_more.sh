set -ex

# openjdk17-jre is needed for jenkins
apk add openjdk17-jre

# apk add cloud-init

## Enable CloudStack in cloud-init
# chown root:root 99_cloudstack.cfg
# chmod 600 99_cloudstack.cfg
# mv 99_cloudstack.cfg /etc/cloud/cloud.cfg.d/99_cloudstack.cfg
# rc-update add cloud-init default

# setup authorized key from CloudStack
chown root:root cloud-set-guest-sshkey.in
chmod 700 cloud-set-guest-sshkey.in
mv cloud-set-guest-sshkey.in /root/cloud-set-guest-sshkey.in
chown root:root cloud-set-guest-sshkey.rc
chmod 700 cloud-set-guest-sshkey.rc
mv cloud-set-guest-sshkey.rc /etc/init.d/cloud-set-guest-sshkey
rc-update add cloud-set-guest-sshkey default
