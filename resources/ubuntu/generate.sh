mkdir http
touch http/meta-data
cp ../resources/ubuntu/user-data http/user-data
cat >$image_name.pkr.hcl <<EOF
packer {
  required_plugins {
    qemu = {
      source = "github.com/hashicorp/qemu"
      version = "~> 1"
    }
  }
}

source "qemu" "ubuntu" {
  accelerator  = "kvm"
  iso_url      = "$image_name.iso"
  iso_checksum = "$iso_checksum"
  ssh_username = "ci"
  ssh_password = "ci"
  ssh_timeout  = "40m"
  disk_size    = 9000
  memory       = 2048
  vm_name      = "$image_name.qcow2"
  headless     = true

  # Location of Cloud-Init / Autoinstall Configuration files
  # Will be served via an HTTP Server from Packer
  http_directory = "http"

  boot_command = [
    "e<wait>",
    "<down><down><down><end>",
    " autoinstall 'ds=nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/'",
    "<f10>"
  ]
}

build {
  sources = ["source.qemu.ubuntu"]

  # Wait till Cloud-Init has finished setting up the image on first-boot
  provisioner "shell" {
    inline = [
      "while [ ! -f /var/lib/cloud/instance/boot-finished ]; do echo 'Waiting for Cloud-Init...'; sleep 10; done"
    ]
  }

  # Setup motd inviting to change the default password
  provisioner "file" {
    source      = "../resources/30-security-warning"
    destination = "30-security-warning"
  }
  provisioner "shell" {
    inline = [
      "sudo chown root:root 30-security-warning",
      "sudo chmod 600 30-security-warning",
      "sudo mv 30-security-warning /etc/update-motd.d/30-security-warning"
    ]
  }

  # Install service that grows disk size at first reboot
  provisioner "file" {
    source      = "../resources/ubuntu/grow_disk_size.sh"
    destination = "grow_disk_size.sh"
  }
  provisioner "file" {
    source      = "../resources/ubuntu/grow_disk_size.service"
    destination = "grow_disk_size.service"
  }
  provisioner "shell" {
    inline = [
      "sudo chown root:root grow_disk_size.sh grow_disk_size.service",
      "sudo chmod 744 grow_disk_size.sh",
      "sudo chmod 644 grow_disk_size.service",
      "sudo mv grow_disk_size.sh /root/grow_disk_size.sh",
      "sudo mv grow_disk_size.service /etc/systemd/system/grow_disk_size.service",
      "sudo systemctl enable grow_disk_size.service"
    ]
  }

  # Enable CloudStack in cloud-init
  # We remove ' autoinstall ...' from /boot/grub/grub.cfg because Ubuntu
  # keeps by default the installation settings where cloud-init was forced
  # to use nocloud-net data source.
  provisioner "file" {
    source      = "../resources/99_cloudstack.cfg"
    destination = "99_cloudstack.cfg"
  }
  provisioner "shell" {
    inline = [
      "sudo chown root:root 99_cloudstack.cfg",
      "sudo chmod 600 99_cloudstack.cfg",
      "sudo mv 99_cloudstack.cfg /etc/cloud/cloud.cfg.d/99_cloudstack.cfg",
      "sudo sed -i 's/ autoinstall .*\$//' /boot/grub/grub.cfg",
      "sudo cloud-init clean --machine-id"
    ]
  }

  # Install service for retrieving CloudStack public SSH key and adding it to
  # ~ci/.ssh/authorized_keys
  provisioner "file" {
    source      = "../resources/cloud-set-guest-sshkey.in"
    destination = "cloud-set-guest-sshkey.in"
  }
  provisioner "file" {
    source      = "../resources/cloud-set-guest-sshkey.service"
    destination = "cloud-set-guest-sshkey.service"
  }
  provisioner "shell" {
    inline = [
      "sudo chown root:root cloud-set-guest-sshkey.in",
      "sudo chmod 700 cloud-set-guest-sshkey.in",
      "sudo mv cloud-set-guest-sshkey.in /root/cloud-set-guest-sshkey.in",
      "sudo chown root:root cloud-set-guest-sshkey.service",
      "sudo chmod 600 cloud-set-guest-sshkey.service",
      "sudo mv cloud-set-guest-sshkey.service /etc/systemd/system/cloud-set-guest-sshkey.service",
      "sudo systemctl enable cloud-set-guest-sshkey"
    ]
  }

  # Install DSI mirrors
  provisioner "shell" {
    inline = [
      "sudo sed -i -e 's|http://.*ubuntu.com|http://si-repository.inria.fr/mirrors|' /etc/apt/sources.list"
    ]
  }

  # Install gitlab-runner
  provisioner "shell" {
    inline = [
      "curl -L 'https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh' | sudo bash",
      "sudo apt-get update",
      "sudo apt-get install gitlab-runner"
    ]
  }

  # Empty home directory
  provisioner "shell" {
    inline = [
      "rm -rf ~/.cache ~/.ssh",
      "unset HISTFILE"
    ]
  }

  # Syncing write before halting the virtual machine
  provisioner "shell" {
    inline = ["sync"]
  }
}
EOF
