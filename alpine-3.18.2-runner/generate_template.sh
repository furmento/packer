set -ex
cp ../alpine-3.18.2/alpine-3.18.2.pkr.hcl alpine-3.18.2-runner.pkr.hcl
sed -i 's/alpine-3.18.2.qcow2/alpine-3.18.2-runner.qcow2/' alpine-3.18.2-runner.pkr.hcl
cp ../alpine-3.18.2/answerfile .
cp ../alpine-3.18.2/setup_in_chroot.sh .
cp ../alpine-3.18.2/grow_disk_size.rc .
ln -s ../alpine-3.18.2/alpine-3.18.2.iso .
